class Zipcode < ActiveRecord::Base
  has_one :store, class_name: "Store", foreign_key: "store_id", primary_key: "store_id"
end
